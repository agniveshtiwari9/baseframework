package com.api.baseframework.exceptions;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

public class ApplicationException extends RuntimeException {

	@JsonIgnore
	private HttpStatus status;
	private String message;
	private String messageDetail;
	private Object data;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageDetail() {
		return messageDetail;
	}
	public void setMessageDetail(String messageDetail) {
		this.messageDetail = messageDetail;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	
	public HttpStatus getStatus() {
		return status;
	}
	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	public ApplicationException(String message, String messageDetail, Object data,HttpStatus status) {
		super(messageDetail);
		this.message = message;
		this.messageDetail = messageDetail;
		this.data = data;
		this.status=status;
	}
	
	
	
}
