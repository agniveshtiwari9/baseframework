package com.api.baseframework.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.api.baseframework.**")
public class RailwaysApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RailwaysApiApplication.class, args);
	}

}

