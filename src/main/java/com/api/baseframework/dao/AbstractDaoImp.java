package com.api.baseframework.dao;

//@Component
//public class AbstractDaoImp<T, Id extends Serializable> implements AbstractDao<T, Id> {
//
//	private Session currentSession;
//
//	private Transaction currentTransaction;
//
//	@PersistenceContext
//	private EntityManager entityManager;
//
//	private Class<T> typeParameterClass;
//
//	@SuppressWarnings("unchecked")
//	public AbstractDaoImp() {
//
//		// this.typeParameterClass=typeParameterClass;
//		typeParameterClass = (Class<T>) (((ParameterizedType) getClass().getGenericSuperclass())
//				.getActualTypeArguments()[1]);
//	}
//
//	@Override
//	public void persist(T entity) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void update(T entity) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public T findById(Id id) {
//		// TODO Auto-generated method stub
//
//		Session session = openCurrentSession();
//		T obj = session.get(typeParameterClass, id);
//
//		return obj;
//	}
//
//	@Override
//	public void delete(T entity) {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public List<T> findAll() {
//		// TODO Auto-generated method stub
//
//		Session session = openCurrentSession();
//		CriteriaBuilder cb = session.getCriteriaBuilder();
//		CriteriaQuery<T> cr = cb.createQuery(typeParameterClass);
//		Root<T> root = cr.from(typeParameterClass);
//		cr.select(root);
//		Query query = session.createQuery(cr);
//		List<T> results = query.getResultList();
//		// closeCurrentSession();
//		return results;
//	}
//
//	@Override
//	public void deleteAll() {
//		// TODO Auto-generated method stub
//
//	}
//
//	private SessionFactory getSessionFactory() {
//
//		return entityManager.getEntityManagerFactory().unwrap(SessionFactory.class);
//	}
//
//	public Session openCurrentSession() {
//
//		currentSession = getSessionFactory().openSession();
//		return currentSession;
//	}
//
//	public Session openCurrentSessionwithTransaction() {
//		currentSession = getSessionFactory().openSession();
//		currentTransaction = currentSession.beginTransaction();
//		return currentSession;
//	}
//
//	public void closeCurrentSession() {
//		currentSession.close();
//	}
//
//	public void closeCurrentSessionwithTransaction() {
//		currentTransaction.commit();
//		currentSession.close();
//	}
//
//	@Override
//	public T saveOrUpdate(T obj) {
//		// TODO Auto-generated method stub
//		Session session = openCurrentSession();
//		session.saveOrUpdate(obj);
//		return obj;
//
//	}
//}
