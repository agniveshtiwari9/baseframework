package com.api.baseframework.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;



/**
 * The Class AbstractDaoImpl. This is the base and generic class for DAO layer
 * to communicate with the database and implementing AbstractDao interface. It
 * will work for Hibernate ORM tool. This is a abstract class and it perform
 * following tasks: 1. Getting session object 2. Getting session object by Key
 * 3. Saving a new entity and updating an existing entity in database 4.
 * Deleting an entity by key 5. Creating criteria query object to create query
 * 6. Getting list of result set using criteria 7. Getting all entity by using
 * criteria query from data base 8. Getting paginated data using criteria from
 * database 9. Evicting object from session using key 10. Counting number of
 * entity available in specified table in database 11. Getting list of entities
 * by using criteria query with like operator and where clause 12. Getting list
 * of entities for specific date using criteria query 13. Getting list of
 * entities for date range using criteria query
 * 
 *
 * 
 */
@Transactional
public abstract class AbstractDaoImpl<PK extends Serializable, T> implements AbstractDao<PK, T> {

	/** The persistent class. */
	private final Class<T> persistentClass;

	/**
	 * Instantiates a new abstract dao impl.
	 */
	@SuppressWarnings("unchecked")
	public AbstractDaoImpl() {
		this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[1];
	}

	/** The entity manager. */
	@PersistenceContext
	public EntityManager entityManager;

	/**
	 * 1. This method is used for getting Session object. 2. Protected method. 3.
	 * Gets the session using Entity Manager.
	 * 
	 * @return Session object
	 */
	protected Session getSession() {
		return entityManager.unwrap(Session.class);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#getByKey(java.io.Serializable)
	 */
	@Override
	public T getByKey(PK key) {
		return (T) getSession().get(persistentClass, key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#saveOrUpdate(java.lang.Object)
	 */
	@Override
	public T saveOrUpdate(T entity) {
		getSession().saveOrUpdate(entity);
		return entity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#delete(java.io.Serializable)
	 */
	@Override
	public T delete(PK key) {
		T t = getByKey(key);
		if (t != null)
			getSession().delete(t);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#createCriteriaQuery()
	 */
	@Override
	public CriteriaQuery<T> createCriteriaQuery() {
		CriteriaQuery<T> criteria = getSession().getCriteriaBuilder().createQuery(persistentClass);
		criteria.from(persistentClass);
		return criteria;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#getListFromCriteria(javax.persistence.
	 * criteria.CriteriaQuery)
	 */
	@Override
	public List<T> getListFromCriteria(CriteriaQuery<T> criteriaQuery) {
		return getSession().createQuery(criteriaQuery).getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#getAll()
	 */
	@Override
	public List<T> getAll() {
		return getListFromCriteria(createCriteriaQuery());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#getAllByPagination(int, int)
	 */
	@Override
	public List<T> getAllByPagination(int index, int pageSize) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(persistentClass);
		Root<T> from = criteriaQuery.from(persistentClass);
		CriteriaQuery<T> select = criteriaQuery.select(from);
		TypedQuery<T> typedQuery = entityManager.createQuery(select);
		typedQuery.setFirstResult(index);
		typedQuery.setMaxResults(pageSize);
		List<T> resultList = typedQuery.getResultList();
		return resultList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#clearObjectFromSession(java.lang.Object)
	 */
	@Override
	public void clearObjectFromSession(T t) {
		getSession().evict(t);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#getCount()
	 */
	@Override
	public Long getCount() {
		CriteriaBuilder qb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);
		cq.select(qb.count(cq.from(persistentClass)));
		return getSession().createQuery(cq).getSingleResult();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#getListByQuery(java.lang.String,
	 * java.lang.String)
	 */
	public List<T> getListByQuery(String query, String columnName) {
		CriteriaBuilder cb = getSession().getCriteriaBuilder();
		CriteriaQuery<T> criteria = getSession().getCriteriaBuilder().createQuery(persistentClass);
		Root<T> root = criteria.from(persistentClass);
		criteria.select(root);
		Predicate predicate = cb.like(root.get(columnName), "%" + query + "%");
		List<T> entityList = getSession().createQuery(criteria.where(predicate)).getResultList();
		return entityList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#getListByDate(java.util.Date,
	 * java.lang.String)
	 */
	@Override
	public List<T> getListByDate(Date date, String columnName) {
		CriteriaBuilder cb = getSession().getCriteriaBuilder();
		CriteriaQuery<T> criteria = getSession().getCriteriaBuilder().createQuery(persistentClass);
		Root<T> root = criteria.from(persistentClass);
		criteria.select(root);
		Predicate predicate = cb.equal(root.get(columnName), date);

		List<T> entityList = getSession().createQuery(criteria.where(predicate)).getResultList();
		return entityList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.necti.base.dao.AbstractDao#getListByFromToDate(java.util.Date,
	 * java.util.Date, java.lang.String)
	 */
	@Override
	public List<T> getListByFromToDate(Date fromDate, Date toDate, String columnName) {
		CriteriaBuilder cb = getSession().getCriteriaBuilder();
		CriteriaQuery<T> criteria = getSession().getCriteriaBuilder().createQuery(persistentClass);
		Root<T> root = criteria.from(persistentClass);
		criteria.select(root);
		Predicate fromDatePredicate = cb.greaterThanOrEqualTo(root.get(columnName), fromDate);
		Predicate toDatePredicate = cb.lessThanOrEqualTo(root.get(columnName), toDate);
		Predicate predicate = cb.and(fromDatePredicate, toDatePredicate);

		List<T> entityList = getSession().createQuery(criteria.where(predicate)).getResultList();
		return entityList;
	}

}