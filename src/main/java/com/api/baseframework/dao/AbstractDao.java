package com.api.baseframework.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.springframework.data.repository.NoRepositoryBean;

/**
 * The Class AbstractDao. This is the base and generic interface for DAO layer
 * to communicate with the database. It performs following tasks: 1. Getting
 * session object 2. Getting session object by Key 3. Saving a new entity and
 * updating an existing entity in database 4. Deleting an entity by key 5.
 * Creating criteria query object to create query 6. Getting list of result set
 * using criteria 7. Getting all entity by using criteria query from data base
 * 8. Getting paginated data using criteria from database 9. Evicting object
 * from session using key 10. Counting number of entity available in specified
 * table in database 11. Getting list of entities by using criteria query with
 * like operator and where clause 12. Getting list of entities for specific date
 * using criteria query 13. Getting list of entities for date range using
 * criteria query
 */
@NoRepositoryBean
public interface AbstractDao<PK extends Serializable, T> {

	/**
	 * 1. This is a generic method. 2. Clears an object from session. 3. The object
	 * to be evicted is identified by passing it as argument.
	 * 
	 * @param t - TypeReference used to identify the object that is to be evicted
	 *          from session
	 */
	public void clearObjectFromSession(T t);

	/**
	 * 1. This is a generic method. 2. Returns session object. 3. The object is
	 * returned on the basis of key passed as argument.
	 * 
	 * @param key - primary key
	 * @return Session object
	 */
	public T getByKey(PK key);

	/**
	 * 1. This is a generic method. 2. Saves a new entity or updates an existing
	 * entity. 3. Generic entity to be saved or updated is passed as an argument.
	 * 
	 * @param entity - Generic Entity
	 * @return Saved or updated entity
	 */
	public T saveOrUpdate(T entity);

	/**
	 * 1. This is a generic method. 2. Deletes specific entity. 3. The entity to be
	 * deleted is identified using key passed as argument.
	 * 
	 * @param id - primary key
	 * @return Deleted entity
	 */
	public T delete(PK id);

	/**
	 * 1. This is a generic method. 2. Returns entity of a specified type. 3. The
	 * returned data will be a list of specific type.
	 * 
	 * @return List
	 */
	public List<T> getAll();

	/**
	 * 1. This is a generic method. 2. Returns paginated data. 3. Uses Criteria
	 * Query to retrieve data on the basis of page number and page size.
	 * 
	 * @param index    - This is the page number
	 * @param pageSize - This is the page size. This indicates how many records for
	 *                 particular page
	 * @return List of specified type
	 */
	public List<T> getAllByPagination(int index, int pageSize);

	/**
	 * 1. This is a generic method. 2. Returns criteria query object using Criteria
	 * Builder. 3. Used to create criteria query.
	 * 
	 * @return CriteriaQuery object - criteria
	 */
	public CriteriaQuery<T> createCriteriaQuery();

	/**
	 * 1. This is a generic method. 2. Returns list of specified entity. 3. Result
	 * is returned from Criteria Query Result Set.
	 * 
	 * @param criteriaQuery - This is the CriteriaQuery object and result set will
	 *                      be extracted from it.
	 * @return List of specified entity
	 */
	public List<T> getListFromCriteria(CriteriaQuery<T> criteriaQuery);

	/**
	 * 1. This is a generic method. 2. Returns List of records from specified table
	 * by using query. 3. Query will be created by using criteria query object,
	 * predicates and it will consist of like operator and where clause.
	 * 
	 * @param query      - This is the query that is going to execute
	 * @param columnName - This is the specific column name that is going to matched
	 *                   with the table column name in database
	 * @return List of specified entity - entityList
	 */
	public List<T> getListByQuery(String query, String columnName);

	/**
	 * 1. This is a generic method. 2. Returns List of records. 3. Records are
	 * returned for specific date from specified table column.
	 * 
	 * @param date       - This is the specific date that is going to matched with
	 *                   the date column of table in database
	 * @param columnName - This is the specific column name that is going to matched
	 *                   with the table column name in database
	 * 
	 * @return List of specified entity - entityList
	 */
	List<T> getListByDate(Date date, String columnName);

	/**
	 * 1. This is a generic method. 2. Returns List of records. 3. Records are
	 * returned between date range from specified table column.
	 * 
	 * @param fromDate   - This is the from date
	 * @param toDate     - This is the to date
	 * @param columnName - This is the specific column name that is going to matched
	 *                   with the table column name in database
	 * @return List of specified entity - entityList
	 */
	public List<T> getListByFromToDate(Date fromDate, Date toDate, String columnName);

	/**
	 * 1. This is a generic method. 2. Counts total number of entities available in
	 * specified table. 3. Uses Criteria Query.
	 * 
	 * @return count
	 */
	public Long getCount();

}
