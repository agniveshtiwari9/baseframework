package com.api.baseframework.dao;

import java.io.Serializable;
import java.util.List;

public interface AbstractDao <T, Id extends Serializable> {
 
    public void persist(T entity);
     
    public void update(T entity);
     
    public T findById(Id id);
     
    public void delete(T entity);
     
    public List<T> findAll();
     
    public void deleteAll();
    
    public void saveOrUpdate(T obj);
     
}
