package com.api.baseframework.response;

import java.util.ResourceBundle;

import com.api.baseframework.utill.ApplicationConstants;

public enum ResponseMessage {

	SUCCESS_RESPONSE;

	public String getMessage() {
		return this.name();
	}

	public String getMessageDetail() {
		ResourceBundle rb = ResourceBundle.getBundle(ApplicationConstants.MESSAGE_PROPERTIES);
		String messageDetail = rb.getString(this.getMessage());
		return messageDetail;
	}
	
	
}
