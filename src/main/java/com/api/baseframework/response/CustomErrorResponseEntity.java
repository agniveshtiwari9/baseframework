package com.api.baseframework.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class CustomErrorResponseEntity extends ResponseEntity<CustomErrorResponse>{

	public CustomErrorResponseEntity(CustomErrorResponse body, HttpStatus status) {
		super(body, status);
	}

}
