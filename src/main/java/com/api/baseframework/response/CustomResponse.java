package com.api.baseframework.response;

public class CustomResponse {
	
	String message;
	String messageDetail;
	Object data;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	
	public String getMessageDetail() {
		return messageDetail;
	}
	public void setMessageDetail(String messageDetail) {
		this.messageDetail = messageDetail;
	}
	public CustomResponse(String message, String messageDetail,Object data) {
		super();
		this.message = message;
		this.data = data;
		this.messageDetail=messageDetail;
	}
	
	
	
		
	
}
