package com.api.baseframework.response;


public class CustomErrorResponse {

	String message;
	String messageDetails;

	public CustomErrorResponse(String message, String messageDetails) {
		super();
		this.message = message;
		this.messageDetails = messageDetails;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessageDetails() {
		return messageDetails;
	}

	public void setMessageDetails(String messageDetails) {
		this.messageDetails = messageDetails;
	}

}
