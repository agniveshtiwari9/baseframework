package com.api.baseframework.exceptionhandler;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.api.baseframework.exceptions.ApplicationException;
import com.api.baseframework.exceptions.NullResponseException;
import com.api.baseframework.response.CustomErrorResponse;
import com.api.baseframework.response.CustomErrorResponseEntity;

@ControllerAdvice
public class ExceptionHandlerClass extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler({NullResponseException.class})
	public CustomErrorResponseEntity NullResponseExceptionResponse(NullResponseException nullResponseException)
	{
		CustomErrorResponse customErrorResponsep=new CustomErrorResponse(nullResponseException.getMessage(),nullResponseException.getMessage());
		CustomErrorResponseEntity customErrorResponseEntity=new CustomErrorResponseEntity(customErrorResponsep, HttpStatus.BAD_REQUEST);
		return customErrorResponseEntity;
	}
	
	@ExceptionHandler({ApplicationException.class})
	public CustomErrorResponseEntity ApplicationExceptionResponse(ApplicationException applicationException)
	{
		CustomErrorResponse customErrorResponsep=new CustomErrorResponse(applicationException.getMessage(),applicationException.getMessageDetail());
		CustomErrorResponseEntity customErrorResponseEntity=new CustomErrorResponseEntity(customErrorResponsep, applicationException.getStatus());
		return customErrorResponseEntity;
	}

	
	
}
