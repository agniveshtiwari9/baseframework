package com.api.baseframework.controller;


import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.api.baseframework.exceptions.ApplicationException;
import com.api.baseframework.response.CustomResponse;
import com.api.baseframework.response.CustomResponseEntity;
import com.api.baseframework.response.ResponseMessage;

@Controller
public class BaseController {

	public CustomResponseEntity bindSuccessResponse(String message, String messageDetail, Object data,
			HttpStatus status) {
		CustomResponse customResponse = new CustomResponse(message, messageDetail, data);
		CustomResponseEntity customResponseEntity = new CustomResponseEntity(customResponse, status);
		return customResponseEntity;

	}

	@RequestMapping(value="/get",method=RequestMethod.GET)
	public ResponseEntity<?> getData() {
//		return bindSuccessResponse(ResponseMessage.SUCCESS_RESPONSE.getMessage(),
//				ResponseMessage.SUCCESS_RESPONSE.getMessageDetail(), null, HttpStatus.OK);
		throw new ApplicationException(ResponseMessage.SUCCESS_RESPONSE.getMessage(), ResponseMessage.SUCCESS_RESPONSE.getMessageDetail(), null, HttpStatus.BAD_REQUEST);
	}

}
